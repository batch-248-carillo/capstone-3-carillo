//Old practice in importing components
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

import { useContext, useState } from 'react';
// import Container from 'react-bootstrap/Container';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import './styleAppNavbar.css';


export default function AppNavbar(){

  //const [ user, setUser ] = useState(localStorage.getItem("email"));
  
  const { user } = useContext(UserContext);
  
  console.log(user);



  return (
    <Navbar expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">AXIS</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/products">Products</Nav.Link>
              
              {(user.isAdmin === true )?
              <Nav.Link as={Link} to="/dash">Dashboard</Nav.Link>
              :
              <>
              </>
              }

              {(user.id !== null)?
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
              </>
            }


              
            
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}