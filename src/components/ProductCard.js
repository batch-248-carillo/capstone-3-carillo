import { Link } from 'react-router-dom';

import '../styleProductCard.css';

export default function ProductCard({product}) {

  return (
    <>
      <Link to={`/products/${product._id}`}>
            <div className="card-wrapper">
              <div className="img-container">
              {/*<img className="img" src={`/images/Muscle Tee.jpg`} alt=""/>*/}
                <img className="img" src={`/images/${product.name}.jpg`} alt=""/>
              </div>
              <div className="text-container">
                <h3 className="product-name">{product.name}</h3>
                <p className="product-brand">{product.brand}</p>
                <p className="product-price">₱ {product.price}</p>
                {/*<p className="product-price">{product.description}</p>*/}
              </div>
            </div>
          </Link>
    </>
  )
}
