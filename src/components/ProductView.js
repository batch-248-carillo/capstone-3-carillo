import { useState, useEffect, useContext } from 'react';
import {Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import './styleProductView.css';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [ product, setProduct ] = useState({});
	const [ quantity, setQuantity ] = useState(1);

	const checkOut = async () => {
		if(localStorage.getItem('token') !== null) {
			try {
				Swal.fire({
				  title: `Would you like to checkout this ${name}? `,
				  showDenyButton: true,
				  confirmButtonText: 'Yes',
				  denyButtonText: 'No',
				}).then(async (result) => {
				  if (result.isConfirmed) {
				    const response = await fetch(`https://capstone-2-carillo-ya7j.onrender.com/orders/createOrder`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
					
							
								productId: productId,
								quantity: quantity
							
						
					})
						})
						if(response.status === 200) {
							Swal.fire({
							title: "Check out Successful",
			                icon: "success",
			                text: `${name} has been added ordered`
						})
						}else{
							Swal.fire({
							title: "Check out Unsuccessful",
			                icon: "error"
						})
							console.log(response.status);
						}
				  } else if (result.isDenied) {
				    Swal.fire({
						title: "Cancelled",
		                icon: "info"
					})
				  }
				})
				
			} catch(error) {
				console.log(error);
			}
		} else {
			Swal.fire({
				title: "Please login",
                icon: "error",
                text: "You have to login first."
			})
		}
	};
	


	useEffect(()=>{


		console.log(productId)

		fetch(`https://capstone-2-carillo-ya7j.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	},[productId])


return (
		<>
			<div className="container">
				<div className="item-container">
					<div className="item-img-wrapper">
						<img className="img" src={`/images/${name}.jpg`}/>
					</div>
					<div className="item-content-wrapper">
						<h2 className="product-title">{name}</h2>
						<p>{description}</p>
						
						<p className="price">₱ {price}</p>
						<div>
							<div>
								<p>Quantity</p>
							</div>
							<div className="quantity-box">
								<button className="btn btn-reduce-quantity" onClick={() => setQuantity(quantity - 1)}>-</button>
								<span>{quantity}</span>
								<button className="btn btn-add-quantity" onClick={() => setQuantity(quantity + 1)}>+</button>
							</div>
						</div>
						<div className="btns-wrapper">
							<button className="btn text-light btn-dark " onClick={checkOut} >Check Out</button>
							
						</div>
					</div>
				</div>
			</div>
		</>
	)

	
	


}