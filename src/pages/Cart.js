import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';


import './styleProducts.css';

export default function Product() {

    // State that will be use to store the product retrieve from the database.
    const {user, setUser} = useContext(UserContext)
    const [product, setProduct] = useState([])
    const [orders, setOrders] = useState([])

    console.log(user.isAdmin)

    useEffect(() => {
        
      
        fetch(`http://localhost:4000/orders/all`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            localStorage.getItem('token')

            // setProduct(data) 

            setProduct(data.map(orders =>{
                return (
                    <ProductCard key={product.id} product = {product}/>

                    )

            
            }))

            // console.log(data);

        })

       

    }, [])

    return (
        <div className="products-content-container">

        
        <div className="products-content">
            <div className="products-content-listing-wrapper">
                <div className="products-content-listing">
                    <header className="products-listing-header">
                        <h1 className="products-listing-h4">
                            Cart 
                        </h1>
                        <h3>{product.length} items</h3>
                    </header>

                    <div className="products-listing-container">
                        <div className="products-grid">
                            {product}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

        )
}

