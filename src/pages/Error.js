import Banner from '../components/Banner';

export default function Error(){
    
    return (
    <>
      <Banner
        title="404 Error: Page Not Found"
        description="The page you're looking for doesn't exist."
        buttonText="Back to Home"
        buttonLink="/"
        buttonVariant="primary"
      />
    </>
  );
}
