import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import './styleHome.css';
// import CourseCard from '../components/CourseCard';


export default function Home(){
	
 return (
   
    <>
      <div className="demo-bg"></div>
      <div className="container">
        <div className="home-wrapper">
          <h1 className="welcome-text">Welcome to <span className="text-accent">AXIS Clothing</span></h1>
        </div>
      </div>
      
    </>
  )
}

