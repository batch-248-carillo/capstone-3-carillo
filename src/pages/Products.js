import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';


import './styleProducts.css';

export default function Product() {

    // State that will be use to store the product retrieve from the database.
    const {user, setUser} = useContext(UserContext)
    const [product, setProduct] = useState([])

    console.log(user.isAdmin)

    useEffect(() => {
        
      
        fetch(`https://capstone-2-carillo-ya7j.onrender.com/products/active`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            localStorage.getItem('token')

            setProduct(data.map(product =>{
                return (
                    <ProductCard key={product.id} product = {product}/>
                    )
            }))
        })

       

    }, [])

    return (

    
        <div className="products-content-container">

        
        <div className="products-content">
            <div className="products-content-listing-wrapper">
                <div className="products-content-listing">
                    <header className="products-listing-header">
                        <h1 className="products-listing-h4">
                            Shop 
                        </h1>
                    </header>

                    <div className="products-listing-container">
                        <div className="products-grid">
                            {product}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

        )
}

